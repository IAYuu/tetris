package tetris;

import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

public class Square{
	protected int x;
	protected int y;
	protected boolean hitRow = false;
	protected boolean hitColumn = false;
	static final int width = 50, height = 50;
	static final int arcWidth = 15, arcHeight = 15;
	Shape shape;
	public Square() {}
	public Square(int x, int y) {
		this.x = x;
		this.y = y;
		shape = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
	}
	public int getRow(){
		return TetrisFrame.row -  y / height - 1;
	}

	public int getColumn() {
		return x / width;
	}

	//更新shape的值
	public void setShape(Shape shape) {
		x = (int)shape.getBounds().getX();
		y = (int)shape.getBounds().getY();
		this.shape = shape;
		//System.out.println("shape: " + this.shape.getBounds().getX() + " , " + this.shape.getBounds().getY());
	}
	
	//碰撞检测
	public void hit() {
		int row = this.getRow();
		int column = this.getColumn();
		hitRow = false;
		hitColumn = false;
		//左移、右移、下移
		if (TetrisFrame.offx < 0 && (x <= 0 || (int)Tetris.rowSquares.get(row).get(column - 1) == 1) || 
				TetrisFrame.offx > 0 && (x >= TetrisFrame.frameWidth - width || (int)Tetris.rowSquares.get(row).get(column + 1) == 1)
				)
			hitRow = true;
		//游戏结束的情况
		if (y < 0 || y <= 0 && (int)Tetris.rowSquares.get(row - 1).get(column) == 1) {
			TetrisFrame.gameOver = true;
		}

		if (y >= TetrisFrame.frameHeight - height || (int)Tetris.rowSquares.get(row - 1).get(column) == 1){
			hitColumn = true;
		}
	}

	//修改方块的位置以及shape更新
	public void updateLocation(int offx, int offy) {
		x += offx;
		y += offy;
		shape = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
	}
}
