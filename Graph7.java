package tetris;

import java.awt.Color;
import java.awt.Graphics2D;


public class Graph7 extends Graph<Graph7>{
	public Graph7() {
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx, centy));
		graph.add(new Square(centx, centy + height));
		graph.add(new Square(centx + width, centy + height));
	}
	
	public void drawGraph(Graphics2D g2d) {
		g2d.setColor(Color.RED);
		super.drawGraph(g2d);
	}

}
