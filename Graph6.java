package tetris;

import java.awt.Color;
import java.awt.Graphics2D;

public class Graph6 extends Graph<Graph6>{
	public Graph6() {
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx - width * 2, centy + height));
		graph.add(new Square(centx, centy + height));
		graph.add(new Square(centx - width, centy + height));
	}
	
	public void drawGraph(Graphics2D g2d) {
		g2d.setColor(new Color(215, 51, 255));
		super.drawGraph(g2d);
	}

}
