package tetris;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public class Graph2 extends Graph<Graph2>{
	public Graph2() {
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx, centy + height));
		graph.add(new Square(centx - width, centy + height));
		graph.add(new Square(centx + width, centy + height));
	}
	
	public void drawGraph(Graphics2D g2d){
		g2d.setColor(Color.BLUE);
		super.drawGraph(g2d);
	}
}
