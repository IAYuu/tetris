package tetris;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Graph4 extends Graph<Graph4>{
	public Graph4() {
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx, centy));
		graph.add(new Square(centx, centy + height));
		graph.add(new Square(centx - width, centy + height));
	}
	public void drawGraph(Graphics2D g2d) {
		g2d.setColor(Color.YELLOW);
		super.drawGraph(g2d);
	}
}

