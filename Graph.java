package tetris;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Graph<T> extends Square{
	/**
	 * @param centx, centy 每个图形初始出现的中心位置
	 *        graph 存放组成该图形的方块的集合
	 *        hitRow, hitColumn 判断该图形在行、列的方向是否发生碰撞
	 */
	protected int centx = TetrisFrame.centx;
	protected int centy = TetrisFrame.centy;
	protected ArrayList<Square> graph = new ArrayList<>();
	public boolean hitRow = false;
	public boolean hitColumn = false;

	//处理碰撞
	public void handHit() {
		//处理碰撞前先把画布填满
		while(Tetris.rowSquares.size() < TetrisFrame.row) {
			ArrayList list = new ArrayList<>();
			while (list.size() < TetrisFrame.column)
				list.add(0);
			Tetris.rowSquares.add(list);
		}
		
		//处理图形的碰撞
		for (Square square : graph) {
			square.hit();
			if (square.hitRow) {
				hitRow = true;
			}
			if (square.hitColumn) {
				hitColumn = true;
			}
		}

		//如果发生行碰撞，则行偏移量置为0
		if (hitRow) {
			TetrisFrame.offx = 0;
		}
		
		//如果发生列碰撞，则先将偏移量置为0
		if (hitColumn) {
			TetrisFrame.offy = 0;
			//处理图形中的每个方块，将所在位置的标志设为1
			for (Square cur : graph) {
				int row = cur.getRow();
				int column = cur.getColumn();
				Tetris.rowSquares.get(row).set(column, 1);
				//判断是否可以得分（也即判断所在行1的数量是否等于列个数）
				int num = 0;
				for (int j = 0; j < TetrisFrame.column; ++j) {
					num = (int)Tetris.rowSquares.get(row).get(j) == 1 ? num + 1 : num; 
				}
				if (num == TetrisFrame.column) {
					Tetris.rowSquares.remove(row);
					TetrisFrame.score += 10;
				}
			}
			return;
		}
		
		//自然下落
		TetrisFrame.offy += height;
	}

	//画出图形
	public void drawGraph(Graphics2D g2d) {
		//先处理图形的碰撞
		this.handHit();
		
		//改变中心位置（因为通过键盘操作会使行、列的偏移量发生改变）
		centx += TetrisFrame.offx;
		centy += TetrisFrame.offy;
		for (int i = 0; i < graph.size(); ++i) {
			graph.get(i).updateLocation(TetrisFrame.offx, TetrisFrame.offy);
			if (TetrisFrame.rotate)
				graph.get(i).setShape(Tetris.rotateShape(graph.get(i).shape, 90, centx, centy + height));
			g2d.fill(graph.get(i).shape);
		}
		
		//每次修改rotate、offx、offy的值后要改回来
		TetrisFrame.rotate = false;
		TetrisFrame.offx = 0;
		TetrisFrame.offy = 0;
	}
}
