package tetris;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public class Graph3 extends Graph<Graph3>{
	public Graph3() {
		graph.add(new Square(centx, centy));
		graph.add(new Square(centx, centy + height));
		graph.add(new Square(centx - width, centy + height));
		graph.add(new Square(centx - width * 2, centy + height));
	}
	public void drawGraph(Graphics2D g2d) {
		g2d.setColor(Color.ORANGE);
		super.drawGraph(g2d);
	}
}
