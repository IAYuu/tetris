package tetris;

import java.awt.Color;
import java.awt.Graphics2D;


public class Graph5 extends Graph<Graph5> {
	public Graph5() {
		graph.add(new Square(centx - width * 2, centy + height));
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx - width, centy + height));
		graph.add(new Square(centx, centy));
	}
	public void draGraph(Graphics2D g2d) {
		g2d.setColor(new Color(100, 135, 51));
		super.drawGraph(g2d);
	}
}
