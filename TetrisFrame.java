package tetris;

import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TetrisFrame extends JPanel {
	static int framex = 600;
	static int framey = 10;
	static int frameWidth = 650;
	static int frameHeight = 1000;
	static int row = 20;
	static int column = 13;
	static int centx = 300;
	static int centy = 0;
	static int offx = 0;
	static int offy = 0;
	static int score = 0;
	static boolean rotate = false;
	static Graph curGraph;
	static boolean gameOver = false;

	public TetrisFrame() {
		Tetris.initTetris();
		JFrame frame = new JFrame();
		frame.setContentPane(this);
		frame.setTitle("俄罗斯方块");
		frame.setLocation(framex, framey);
		frame.setSize(672, 1056);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//添加键盘监听
		frame.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					TetrisFrame.offx = -Square.width;break;
				case KeyEvent.VK_RIGHT:
					TetrisFrame.offx = Square.width;break;
				case KeyEvent.VK_UP: {
					rotate = true;break;
				}
				case KeyEvent.VK_DOWN:
					TetrisFrame.offy = Square.height;break;
				default:
					break;
				}
			}
		});
		frame.setVisible(true);
		System.out.println(frame.getInsets());
		System.out.println(this.getWidth() + " , " + this.getHeight());
		System.out.println(frame.getWidth() + " , " + frame.getHeight());
	}

	public void launchFrame(){
		new PaintThread().start();
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		//防止锯齿
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		//计算分数
		g2d.setFont(new Font("Helvetica", Font.BOLD, 30));
		g2d.drawString("SCORE: " + score, 100, 100);

		//画出正在下落的图形
		if (curGraph == null || curGraph.hitColumn)
			curGraph = Tetris.getRandomGraph();
		curGraph.drawGraph(g2d);

		//画出集合里的所有图形
		for (int i = 0; i < Tetris.rowSquares.size(); ++i) {
			for (int j = 0; j < Tetris.rowSquares.get(i).size(); ++j)
				if ((int)Tetris.rowSquares.get(i).get(j) == 1) {
					g2d.setColor(new Color(137, 255, 51));
					g2d.fillRoundRect(j * Square.width, (row - i - 1) * Square.height,
							Square.width, Square.height, Square.arcWidth, Square.arcHeight);
				}
		}
	}

	class PaintThread extends Thread {
		public void run () {
			while(true) {
				repaint();
				if (gameOver) {
					//游戏结束后跳出选择对话框
					Object[] options = {"再来一局", "不玩了"};
					int m = JOptionPane.showOptionDialog(null, "分数为：" + score + ", 请选择：", "游戏结束", JOptionPane.YES_NO_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (m == 1) {
						//退出程序
						System.exit(0);
					} else { 
						//重来一局
						gameOver = false;
						Tetris.rowSquares.clear();
						score = 0;
					}

				}
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
	

	public static void main(String[] args) {
		TetrisFrame tetrisFrame = new TetrisFrame();
		tetrisFrame.launchFrame();
	}

}
