package tetris;

import java.awt.Color;
import java.awt.Graphics2D;

public class Graph1 extends Graph<Graph1>{
	public Graph1() {
		graph.add(new Square(centx - width * 2,  centy));
		graph.add(new Square(centx - width, centy));
		graph.add(new Square(centx, centy));
		graph.add(new Square(centx + width, centy));
	}
	public void drawGraph(Graphics2D g2d) {
		g2d.setColor(Color.CYAN);
		super.drawGraph(g2d);
	}

}
