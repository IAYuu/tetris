package tetris;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.lang.reflect.Field;
import java.util.ArrayList;


import javafx.scene.transform.Shear;

//处理俄罗斯方块的功能
public class Tetris {

	/**
	 * @param graphs 存放七种图形的集合
	 *        rowSuares 存放界面中每行的方块，用于碰撞检测
	 */
	static ArrayList<Graph> graphs;
	static ArrayList<ArrayList> rowSquares;
	public Tetris() {}
	//将七种图形存入集合内
	public static void initTetris() {
		graphs = new ArrayList<Graph>();
		rowSquares = new ArrayList<>();

		Graph<Graph1> graph1 = new Graph1();
		Graph<Graph2> graph2 = new Graph2();
		Graph<Graph3> graph3 = new Graph3();
		Graph<Graph4> graph4 = new Graph4();
		Graph<Graph5> graph5 = new Graph5();
		Graph<Graph6> graph6 = new Graph6();
		Graph<Graph7> graph7 = new Graph7();

		graphs.add(graph1);
		graphs.add(graph2);
		graphs.add(graph3);
		graphs.add(graph4);
		graphs.add(graph5);
		graphs.add(graph6);
		graphs.add(graph7);

	}

	//图形的旋转
	public static final Shape rotateShape(Shape shape, double rotation, int centx, int centy) {
		AffineTransform transform = new AffineTransform();
		transform.rotate(Math.toRadians(rotation), centx, centy);
		return transform.createTransformedShape(shape);
	}

	//获得新出现的图形
	public static Graph getRandomGraph() {
		try {
			int k = (int)(Math.random() * graphs.size());
			Class clazz = graphs.get(k).getClass();
			assert graphs != null : "graph is null";
			//通过反射获得该图形类的一个新实例
			graphs.set(k, (Graph) clazz.newInstance());
			return  graphs.get(k);
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
