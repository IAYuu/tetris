CC=javac
FILES=$(wildcard *.java)

all:
	$(CC) -encoding gbk -d . $(FILES)
	java tetris/TetrisFrame

clean:
	@rm -f *.class
